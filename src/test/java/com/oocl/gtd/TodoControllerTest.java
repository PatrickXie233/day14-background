package com.oocl.gtd;

import com.oocl.gtd.entity.Todo;
import com.oocl.gtd.repository.TodoRepository;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class TodoControllerTest {
    @Autowired
    private TodoRepository todoRepository;
    @Autowired
    private MockMvc client;

    @BeforeEach
    void prepareData() {
        todoRepository.deleteAll();

    }

    @Test
    void should_return_todos_preform_get_given_todos_in_db() throws Exception {
//        given
        Todo todo1 = new Todo("todo1", false);
        Todo todo2 = new Todo("todo2", false);
//        when
        todoRepository.saveAll(List.of(todo1, todo2));
//        then
        client.perform(MockMvcRequestBuilders.get("/todos"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].text").value((todo1.getText())))
                .andExpect(jsonPath("$[0].done").value((todo1.isDone())))
                .andExpect(jsonPath("$[1].text").value((todo2.getText())))
                .andExpect(jsonPath("$[1].done").value((todo2.isDone())));
    }

    @Test
    void should_return_todo_when_post_given_todo() throws Exception {
//        given
        String newTodoJson = "{" +
                "\"text\":\"test post\"," +
                "\"done\":false}";
//        when
        client.perform(MockMvcRequestBuilders.post("/todos")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(newTodoJson))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.text").value("test post"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.done").value(false));
//        should
        List<Todo> todos = todoRepository.findAll();
        assertThat(todos,hasSize(1));
        assertThat(todos.get(0).getText(),equalTo("test post"));
        assertThat(todos.get(0).isDone(),equalTo(false));
    }
    @Test
    void should_delete_todo_when_delete_given_id() throws Exception {
//        given
        Todo todo = new Todo("todo", false);
//        when
        Todo saveTodo = todoRepository.save(todo);
        client.perform(MockMvcRequestBuilders.delete("/todos/{id}",saveTodo.getId()))
                .andExpect(MockMvcResultMatchers.status().isNoContent());
        assertThat(todoRepository.findAll(), empty());
    }
    @Test
    void should_update_todo_text_when_put_given_id_and_new_todo() throws Exception {
//        given
        Todo todo1 = new Todo("todo", false);
        Todo saveTodoText = todoRepository.save(todo1);
        String updateTodoJson = "{\n" +
                "    \"text\": \"update todo done\",\n" +
                "    \"done\": true\n" +
                "}";
//        when
        client.perform(MockMvcRequestBuilders.put("/todos/{id}",saveTodoText.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(updateTodoJson))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(saveTodoText.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.text").value("update todo done"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.done").value(true));
    }

    @Test
    void should_return_todo_when_get_given_id() throws Exception {
        //        given
        Todo todo = new Todo("todo1", false);
//        when
        todoRepository.save(todo);
//        then
        client.perform(MockMvcRequestBuilders.get("/todos/{id}",todo.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value((todo.getId())))
                .andExpect(jsonPath("$.done").value((todo.isDone())))
                .andExpect(jsonPath("$.text").value((todo.getText())));
    }
}
