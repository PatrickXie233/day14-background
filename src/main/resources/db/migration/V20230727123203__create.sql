
CREATE TABLE if not exists `todo` (
  `id` INT AUTO_INCREMENT NOT NULL ,
  `text` VARCHAR(255) NOT NULL,
  `done` TINYINT NOT NULL,
  PRIMARY KEY (`id`));
