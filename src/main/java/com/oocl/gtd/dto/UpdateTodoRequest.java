package com.oocl.gtd.dto;

public class UpdateTodoRequest {
    private String text;
    private Boolean done;
    public UpdateTodoRequest(String text, Boolean done) {
        this.text = text;
        this.done = done;
    }

    public UpdateTodoRequest() {
    }

    public Boolean getDone() {
        return done;
    }

    public void setDone(Boolean done) {
        this.done = done;
    }


    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
