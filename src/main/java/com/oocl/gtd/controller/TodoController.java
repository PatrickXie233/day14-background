package com.oocl.gtd.controller;

import com.oocl.gtd.dto.TodoResponse;
import com.oocl.gtd.dto.UpdateTodoRequest;
import com.oocl.gtd.entity.Todo;
import com.oocl.gtd.service.TodoService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/todos")
public class TodoController {
    private final TodoService todoService;

    public TodoController(TodoService todoService) {
        this.todoService = todoService;
    }
    @GetMapping
    public List<TodoResponse> getTodoList(){
        return todoService.getTodoList();
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public TodoResponse addTodo(@RequestBody Todo todo){
        return todoService.addTodoItem(todo);
    }
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteEmployee(@PathVariable int id) {
        todoService.deleteTodo(id);
    }

    @PutMapping("/{id}")
    public TodoResponse updateTodo(@PathVariable int id,@RequestBody UpdateTodoRequest updateTodoRequest) {
        return todoService.updateTodoText(id, updateTodoRequest);
    }
    @GetMapping("/{id}")
    public TodoResponse getTodoById(@PathVariable int id){
        return todoService.getTodoById(id);
    }
}
