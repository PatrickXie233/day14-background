package com.oocl.gtd.service;

import com.oocl.gtd.dto.TodoResponse;
import com.oocl.gtd.dto.UpdateTodoRequest;
import com.oocl.gtd.entity.Todo;
import com.oocl.gtd.exception.TodoNotFoundException;
import com.oocl.gtd.repository.TodoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.oocl.gtd.mapper.TodoMapper.entityToTodoListResponse;
import static com.oocl.gtd.mapper.TodoMapper.entityToTodoResponse;

@Service
public class TodoService {
    @Autowired
    private TodoRepository todoRepository;
    public List<TodoResponse> getTodoList() {
        return entityToTodoListResponse(todoRepository.findAll());
    }

    public TodoResponse addTodoItem(Todo todo) {
        return entityToTodoResponse(todoRepository.save(todo)) ;
    }

    public void deleteTodo(int id) {
        todoRepository.deleteById(id);
    }

    public TodoResponse updateTodoText(int id, UpdateTodoRequest updateTodoRequest) {
        Todo newTodo = new Todo(id, updateTodoRequest.getText(), updateTodoRequest.getDone());
        return entityToTodoResponse(todoRepository.save(newTodo));
    }

    public TodoResponse getTodoById(int id) {
        return entityToTodoResponse(todoRepository.findById(id).orElseThrow(TodoNotFoundException::new));
    }
}
