package com.oocl.gtd.mapper;

import com.oocl.gtd.dto.TodoResponse;
import com.oocl.gtd.entity.Todo;
import org.springframework.beans.BeanUtils;

import java.util.List;
import java.util.stream.Collectors;

public class TodoMapper {
    public static List<TodoResponse> entityToTodoListResponse(List<Todo> todoList) {
        return todoList.stream().map(TodoMapper::entityToTodoResponse).collect(Collectors.toList());
    }

    public static TodoResponse entityToTodoResponse(Todo todo) {
        TodoResponse todoResponse = new TodoResponse();
        BeanUtils.copyProperties(todo, todoResponse);
        return todoResponse;
    }

}
