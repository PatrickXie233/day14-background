package com.oocl.gtd.exception;

public class TodoNotFoundException extends RuntimeException{
    public TodoNotFoundException() {
        super("todo id not found");
    }

}
